// Node.js Introduction

// Use the "require" directive load Node.js modules
// the "http module" lets Node.js transfer data using the Hyper Text Transfer Protocol (HTTP)
// HTTP is a protocol that allows the fetching of resources such as HTML documents.

// The message sent by the "client", usually a Web Browser, called "request"
// The message sent by the "server" as an asnwers are called "respons"

let http = require("http");

// Using this module's createServer() method, we can create an HTTP server that listen to requests on a specified port and gives responses back to client.

// A port is a virtual point where network connections start and end. Each port is associated with a specific process or server.

// http.createServer(function (request, response) {

//     // Use the writeHead() method to:
//     // To set a status code for the response - a 200 means OK
//     // Set the content-type of the response as a plain text message

//     response.writeHead(200, { 'Content-Type': 'text/plain' });

//     // Send the response with text content:
//     response.end("Welcome to my Page");

//     // The server will be assigned to port 4000 via the "listen(4000)" method where server will listen to any request that are sent to it eventually communicating with our server.
// }).listen(4000)

// // When server is runninh, console will print messages
// console.log('Server is running at localhost:4000')

// =========== Part 2 ============ //
// With conditionals, i.e. localhost:4000/greeting
const server = http.createServer((request, response) => {

    // Accessing the "greeting" route returns a message of "Hello BPI"
    // "request" is object that is sent via the client (browser).
    // The "url" property refers to the url or link in the browser.
    if (request.url === '/greeting') {
        response.writeHead(200, { 'Content-Type': 'text/plain' });
        response.end("Welcome to BPI");
    } else if (request.url === '/customer-service') {
        response.writeHead(200, { 'Content-Type': 'text/plain' });
        response.end("Welcome to Customer Service of BPI");
    } else {
        response.writeHead(404, { 'Content-Type': 'text/plain' });
        response.end("Page is not available");
    }
}).listen(4000)

console.log('Server running at localhost:4000')